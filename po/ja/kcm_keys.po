msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-07 02:44+0000\n"
"PO-Revision-Date: 2020-05-03 20:07-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: globalaccelmodel.cpp:210
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr ""

#: globalaccelmodel.cpp:322
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr ""

#: globalaccelmodel.cpp:372
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr ""

#: kcm_keys.cpp:57
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr ""

#: kcm_keys.cpp:312
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""

#: kcm_keys.cpp:316
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""

#: kcm_keys.cpp:317
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr ""

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "File"
msgstr ""

#: standardshortcutsmodel.cpp:35
#, kde-format
msgid "Edit"
msgstr ""

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "Navigation"
msgstr ""

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "View"
msgstr ""

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Settings"
msgstr ""

#: standardshortcutsmodel.cpp:40
#, kde-format
msgid "Help"
msgstr ""

#: ui/main.qml:27
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:27
#, kde-format
msgid "Commands"
msgstr ""

#: ui/main.qml:27
#, kde-format
msgid "System Settings"
msgstr ""

#: ui/main.qml:27
#, kde-format
msgid "Common Actions"
msgstr ""

#: ui/main.qml:43
#, kde-format
msgctxt "@action: button Import shortcut scheme"
msgid "Import…"
msgstr ""

#: ui/main.qml:48
#, kde-format
msgctxt "@action:button"
msgid "Cancel Export"
msgstr ""

#: ui/main.qml:49
#, kde-format
msgctxt "@action:button Export shortcut scheme"
msgid "Export…"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr ""

#: ui/main.qml:87
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""

#: ui/main.qml:93
#, kde-format
msgctxt "@action:button Save shortcut scheme"
msgid "Save Scheme"
msgstr ""

#: ui/main.qml:158
#, kde-format
msgctxt "@action:button Add new shortcut"
msgid "Add New"
msgstr ""

#: ui/main.qml:164
#, kde-format
msgctxt "@action:menu End of the sentence 'Add New Application…'"
msgid "Application…"
msgstr ""

#: ui/main.qml:170
#, kde-format
msgctxt "@action:menu End of the sentence 'Add New Command or Script…'"
msgid "Command or Script…"
msgstr ""

#: ui/main.qml:222
#, kde-format
msgctxt "@tooltip:button %1 is the text of a custom command"
msgid "Edit command for %1"
msgstr ""

#: ui/main.qml:238
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr ""

#: ui/main.qml:249
#, kde-format
msgid "Undo deletion"
msgstr ""

#: ui/main.qml:302
#, kde-format
msgid "No items matched the search terms"
msgstr ""

#: ui/main.qml:335
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr ""

#: ui/main.qml:347
#, kde-format
msgid "Export Shortcut Scheme"
msgstr ""

#: ui/main.qml:347 ui/main.qml:463
#, kde-format
msgid "Import Shortcut Scheme"
msgstr ""

#: ui/main.qml:349
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr ""

#: ui/main.qml:377
#, kde-format
msgid "Edit Command"
msgstr ""

#: ui/main.qml:377
#, kde-format
msgid "Add Command"
msgstr ""

#: ui/main.qml:395
#, kde-format
msgid "Save"
msgstr ""

#: ui/main.qml:395
#, kde-format
msgid "Add"
msgstr ""

#: ui/main.qml:420
#, kde-format
msgid "Enter a command or choose a script file:"
msgstr ""

#: ui/main.qml:434
#, kde-format
msgctxt "@action:button"
msgid "Choose…"
msgstr ""

#: ui/main.qml:447
#, kde-format
msgctxt "@title:window"
msgid "Choose Script File"
msgstr ""

#: ui/main.qml:449
#, kde-format
msgctxt "Template for file dialog"
msgid "Script file (*.*sh)"
msgstr ""

#: ui/main.qml:470
#, kde-format
msgid "Select the scheme to import:"
msgstr ""

#: ui/main.qml:487
#, kde-format
msgid "Custom Scheme"
msgstr ""

#: ui/main.qml:492
#, kde-format
msgid "Select File…"
msgstr ""

#: ui/main.qml:492
#, kde-format
msgid "Import"
msgstr ""

#: ui/ShortcutActionDelegate.qml:29
#, kde-format
msgid "Editing shortcut: %1"
msgstr ""

#: ui/ShortcutActionDelegate.qml:41
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr ""

#: ui/ShortcutActionDelegate.qml:54
#, kde-format
msgid "No active shortcuts"
msgstr ""

#: ui/ShortcutActionDelegate.qml:95
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] ""

#: ui/ShortcutActionDelegate.qml:97
#, kde-format
msgid "No default shortcuts"
msgstr ""

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr ""

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr ""

#: ui/ShortcutActionDelegate.qml:127
#, kde-format
msgid "Custom shortcuts"
msgstr ""

#: ui/ShortcutActionDelegate.qml:153
#, kde-format
msgid "Delete this shortcut"
msgstr ""

#: ui/ShortcutActionDelegate.qml:159
#, kde-format
msgid "Add custom shortcut"
msgstr ""

#: ui/ShortcutActionDelegate.qml:196
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr ""
