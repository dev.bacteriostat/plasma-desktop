# translation of plasma_applet_pager.po to Lithuanian
# This file is distributed under the same license as the plasma_applet_pager package.
# Gintautas Miselis <gintautas@miselis.lt>, 2008.
# Donatas G. <dgvirtual@akl.lt>, 2010.
# liudas@aksioma.lt <liudas@aksioma.lt>, 2014.
# Liudas Ališauskas <liudas@akmc.lt>, 2015.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_pager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-01 02:16+0000\n"
"PO-Revision-Date: 2022-09-19 12:36+0300\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.1.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Bendra"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr "Bendra:"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr "Langų kontūruose rodyti programų piktogramas"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr "Rodyti tik dabartinį ekraną"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr "Cikliškai perjunginėti"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "Išdėstymas:"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "Numatytasis"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "Horizontaliai"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "Vertikaliai"

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr "Teksto rodymas:"

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "Jokio teksto"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "Veiklos numeris"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "Darbalaukio numeris"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "Veiklos pavadinimas"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "Darbalaukio pavadinimas"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr "Pasirinkus dabartinę veiklą:"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "Pasirinkus dabartinį virtualų darbalaukį:"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "Nieko nedaroma"

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr "Parodomas darbalaukis"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "…ir dar %1 langas"
msgstr[1] "…ir dar %1 langai"
msgstr[2] "…ir dar %1 langų"
msgstr[3] "…ir dar %1 langas"

#: package/contents/ui/main.qml:352
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "%1 langas:"
msgstr[1] "%1 langai:"
msgstr[2] "%1 langų:"
msgstr[3] "%1 langas:"

#: package/contents/ui/main.qml:364
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "%1 suskleistas langas:"
msgstr[1] "%1 suskleisti langai:"
msgstr[2] "%1 suskleistų langų:"
msgstr[3] "%1 suskleistas langas:"

#: package/contents/ui/main.qml:439
#, kde-format
msgid "Desktop %1"
msgstr "Darbalaukis %1"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "Perjungti į %1"

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Show Activity Manager…"
msgstr "Rodyti veiklų tvarkytuvę…"

#: package/contents/ui/main.qml:591
#, kde-format
msgid "Add Virtual Desktop"
msgstr "Pridėti virtualų darbalaukį"

#: package/contents/ui/main.qml:597
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "Šalinti virtualų darbalaukį"

#: package/contents/ui/main.qml:604
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "Konfigūruoti virtualius darbalaukius…"

#~ msgid "Icons"
#~ msgstr "Ženkliukus"
