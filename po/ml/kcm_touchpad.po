# Malayalam translations for plasma-desktop package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-08 01:39+0000\n"
"PO-Revision-Date: 2019-12-12 22:24+0000\n"
"Last-Translator: Vivek KJ Pazhedath <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr ""

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr ""

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr ""

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr ""

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr ""

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr ""

#: backends/x11/xlibbackend.cpp:84 kcm/libinput/touchpad.qml:94
#, kde-format
msgid "No touchpad found"
msgstr ""

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr ""

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr ""

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr ""

#: kcm/libinput/touchpad.qml:108
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/touchpad.qml:134
#, kde-format
msgid "General:"
msgstr ""

#: kcm/libinput/touchpad.qml:135
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/touchpad.qml:139
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/touchpad.qml:163
#, kde-format
msgid "Disable while typing"
msgstr ""

#: kcm/libinput/touchpad.qml:167
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr ""

#: kcm/libinput/touchpad.qml:191
#, kde-format
msgid "Left handed mode"
msgstr ""

#: kcm/libinput/touchpad.qml:195
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/touchpad.qml:219
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr ""

#: kcm/libinput/touchpad.qml:223 kcm/libinput/touchpad.qml:864
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/touchpad.qml:254
#, kde-format
msgid "Pointer speed:"
msgstr ""

#: kcm/libinput/touchpad.qml:352
#, kde-format
msgid "Pointer acceleration:"
msgstr ""

#: kcm/libinput/touchpad.qml:383
#, kde-format
msgid "None"
msgstr ""

#: kcm/libinput/touchpad.qml:387
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr ""

#: kcm/libinput/touchpad.qml:396
#, kde-format
msgid "Standard"
msgstr ""

#: kcm/libinput/touchpad.qml:400
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr ""

#: kcm/libinput/touchpad.qml:415
#, kde-format
msgid "Tapping:"
msgstr ""

#: kcm/libinput/touchpad.qml:416
#, kde-format
msgid "Tap-to-click"
msgstr ""

#: kcm/libinput/touchpad.qml:420
#, kde-format
msgid "Single tap is left button click."
msgstr ""

#: kcm/libinput/touchpad.qml:449
#, kde-format
msgid "Tap-and-drag"
msgstr ""

#: kcm/libinput/touchpad.qml:453
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr ""

#: kcm/libinput/touchpad.qml:480
#, kde-format
msgid "Tap-and-drag lock"
msgstr ""

#: kcm/libinput/touchpad.qml:484
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr ""

#: kcm/libinput/touchpad.qml:504
#, kde-format
msgid "Two-finger tap:"
msgstr ""

#: kcm/libinput/touchpad.qml:515
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr ""

#: kcm/libinput/touchpad.qml:516
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr ""

#: kcm/libinput/touchpad.qml:518
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr ""

#: kcm/libinput/touchpad.qml:519
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr ""

#: kcm/libinput/touchpad.qml:521
#, kde-format
msgid "Right-click"
msgstr ""

#: kcm/libinput/touchpad.qml:522
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr ""

#: kcm/libinput/touchpad.qml:524
#, kde-format
msgid "Middle-click"
msgstr ""

#: kcm/libinput/touchpad.qml:525
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr ""

#: kcm/libinput/touchpad.qml:584
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/touchpad.qml:613
#, kde-format
msgid "Two fingers"
msgstr ""

#: kcm/libinput/touchpad.qml:617
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr ""

#: kcm/libinput/touchpad.qml:625
#, kde-format
msgid "Touchpad edges"
msgstr ""

#: kcm/libinput/touchpad.qml:629
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr ""

#: kcm/libinput/touchpad.qml:639
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr ""

#: kcm/libinput/touchpad.qml:655
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/touchpad.qml:663 kcm/libinput/touchpad.qml:680
#, kde-format
msgid "Disable horizontal scrolling"
msgstr ""

#: kcm/libinput/touchpad.qml:688
#, kde-format
msgid "Scrolling speed:"
msgstr ""

#: kcm/libinput/touchpad.qml:739
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/touchpad.qml:745
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/touchpad.qml:755
#, kde-format
msgid "Right-click:"
msgstr ""

#: kcm/libinput/touchpad.qml:789
#, kde-format
msgid "Press bottom-right corner"
msgstr ""

#: kcm/libinput/touchpad.qml:793
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr ""

#: kcm/libinput/touchpad.qml:801
#, kde-format
msgid "Press anywhere with two fingers"
msgstr ""

#: kcm/libinput/touchpad.qml:805
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr ""

#: kcm/libinput/touchpad.qml:819
#, kde-format
msgid "Middle-click: "
msgstr ""

#: kcm/libinput/touchpad.qml:848
#, kde-format
msgid "Press bottom-middle"
msgstr ""

#: kcm/libinput/touchpad.qml:852
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr ""

#: kcm/libinput/touchpad.qml:860
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr ""

#: kcm/libinput/touchpad.qml:873
#, kde-format
msgid "Press anywhere with three fingers"
msgstr ""

#: kcm/libinput/touchpad.qml:879
#, kde-format
msgid "Press anywhere with three fingers."
msgstr ""

#: kcm/touchpadconfig.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/touchpadconfig.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr ""

#: kcm/touchpadconfig.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/touchpadconfig.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/touchpadconfig.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/touchpadconfig.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr ""

#: kcm/touchpadconfig.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr ""

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr ""

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr ""

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr ""
#~ "shijualexonline@gmail.com,snalledam@dataone.in,vivekkj2004@gmail.com"
